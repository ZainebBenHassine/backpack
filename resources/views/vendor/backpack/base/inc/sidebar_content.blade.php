<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

{{--@role('member')--}}
{{--    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-tasks'></i> Tags</a></li>--}}
{{--    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-business-time'></i> Catégories</a></li>--}}
{{--    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon la la-product-hunt'></i> Produits</a></li>--}}

{{--    <li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}"><i class="nav-icon la la-newspaper-o"></i> <span>Articles</span></a></li>--}}
{{--    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('archive') }}'><i class='nav-icon la la-question'></i> Archives</a></li>--}}
{{--@endrole--}}

{{--@role('admin')--}}
{{--    <li class="nav-item nav-dropdown">--}}
{{--        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i> Authentication</a>--}}
{{--        <ul class="nav-dropdown-items">--}}
{{--            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>--}}
{{--        </ul>--}}
{{--    </li>--}}
{{--@endrole--}}


    <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-group"></i>Permissions</a>
        <ul class="nav-dropdown-items">
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-group"></i> <span>Roles</span></a></li>
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
        </ul>
    </li>



{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-business-time'></i> Catégories</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon la la-product-hunt'></i> Produits</a></li>--}}

{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}"><i class="nav-icon la la-newspaper-o"></i> <span>Articles</span></a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('archive') }}'><i class='nav-icon la la-question'></i> Archives</a></li>


{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('dummy') }}'><i class='nav-icon la la-question'></i> Dummies</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('monster') }}'><i class='nav-icon la la-question'></i> Monsters</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('icon') }}'><i class='nav-icon la la-question'></i> Icons</a></li>--}}
{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('role') }}'><i class='nav-icon la la-question'></i> Roles</a></li>--}}

{{--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('tag') }}'><i class='nav-icon la la-tasks'></i> Tags</a></li>--}}
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-list'></i> Catégories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('session') }}'><i class='nav-icon la la-tasks'></i> Sessions</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('level') }}'><i class='nav-icon la la-level-up'></i> Niveaux</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('formation') }}'><i class='nav-icon la la-chalkboard-teacher'></i> Formations_V1</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('formationv2') }}'><i class='nav-icon la la-chalkboard-teacher'></i> Formations_V2</a></li>
{{--<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>File manager</span></a></li>--}}

<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
