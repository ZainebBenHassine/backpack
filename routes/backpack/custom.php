<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.



Route::get('api/article', 'App\Http\Controllers\Api\ArticleController@index');
Route::get('api/article/{id}', 'App\Http\Controllers\Api\ArticleController@show');
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('tag', 'TagCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('article', 'ArticleCrudController');

    Route::get('charts/weekly-users', 'Charts\WeeklyUsersChartController@response')->name('charts.weekly-users.index');

    Route::crud('archive', 'ArchiveCrudController');

    Route::crud('formation', 'FormationCrudController');

    Route::crud('dummy', 'DummyCrudController');
    Route::crud('monster', 'MonsterCrudController');
    Route::crud('icon', 'IconCrudController');
    Route::crud('role', 'RoleCrudController');
    Route::crud('session', 'SessionCrudController');
    Route::crud('formationv2', 'FormationV2CrudController');
    Route::crud('level', 'LevelCrudController');


    Route::get('api/article', 'App\Http\Controllers\Api\ArticleController@index');
    Route::get('api/article/{id}', 'App\Http\Controllers\Api\ArticleController@show');
});
