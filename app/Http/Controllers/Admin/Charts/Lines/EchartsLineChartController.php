<?php

namespace App\Http\Controllers\Admin\Charts\Lines;

use App\User;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Echarts\Chart;

class EchartsLineChartController extends ChartController
{
    public function setup()
    {
        $userCount = User::count();
        $this->chart = new Chart();

        $this->chart->dataset('Red', 'line',[$userCount] )
                    ->color('rgba(205, 32, 31, 1)');

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels(['One', 'Two', 'Three', 'Four']);
    }
}
