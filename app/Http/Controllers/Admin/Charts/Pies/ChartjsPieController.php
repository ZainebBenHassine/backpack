<?php

namespace App\Http\Controllers\Admin\Charts\Pies;

use App\User;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class ChartjsPieController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        $userCount = User::count();
        $categoryCount = \App\Models\Category::count();
        $formationCount = \App\Models\Formation::count();
        $this->chart->dataset('Red', 'pie', [$userCount, $categoryCount, $formationCount])
                    ->backgroundColor([
                        'rgb(70, 127, 208)',
                        'rgb(66, 186, 150)',
                        'rgb(96, 92, 168)',
                    ]);

        // OPTIONAL
        $this->chart->displayAxes(false);
        $this->chart->displayLegend(true);

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels(['userCount', 'categoryCount', 'formationCount']);
    }
}
