<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\FormationV2Request;
use App\Models\Category;
use App\Models\Session;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class FormationV2CrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FormationV2CrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\InlineCreateOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\FormationV2::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/formationv2');
        CRUD::setEntityNameStrings('formation_v2', 'formations_v2s');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        //CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
        CRUD::addColumn([
            'label' => 'Session',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'name' ,

        ]);

        CRUD::addColumn([
            'label' => 'Date de début',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'start_date' ,
        ]);

        CRUD::addColumn([
            'label' => 'Catégorie',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'categories',
            'attribute' => 'name',
        ]);



        CRUD::addColumn([   // Table
            'name'            => 'options',
            'label'           => 'Formations',
            'type'            => 'table',
            'columns'         => [
                'title'  => 'Titre',
                'nb_hours' => 'Nb. d\'heure',
                'description'  => 'Description',
            ],
            'max' => 5, // maximum rows allowed in the table
            'min' => 0, // minimum rows allowed in the table
        ]);





        /**
         *
         *  Create filter by created_at
         */

        $this->crud->addFilter([
            'label' => 'Date de création',
            'type' => 'date_range',
            'name' => 'created_at',
            'date_range_options' => [
                'timePicker' => true,
                'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ],
        ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from . " 00:00:00");
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . " 23:59:59");
                //Or
                //$this->crud->addClause('where', 'created_at', '<=',date("Y-m-d", strtotime($dates->to) + (3600*24) ));
            });


        /**
         *
         *  Create filter by titre
         */
        $this->crud->addFilter(
            [ // text filter
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Titre',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'title', 'LIKE', "%$value%");
            }
        );



        /**
         *
         *  Create a select2 filter by categories
         */
        $this->crud->addFilter([
            'name'  => 'categories',
            'type'  => 'select2_multiple',
            'label' => 'Catégories_V2'
        ], function() { // the options that show up in the select2
            return Category::all()->pluck('name', 'id')->toArray();
        }, function($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('categories', function ($query) use ($value) {
                    $query->where('category_id', $value);
                });
            }
        });


        /**
         *
         *  Create filter by sessions
         */
        $this->crud->addFilter([
            'name'  => 'sessions',
            'type'  => 'select2_multiple',
            'label' => 'Sessions'
        ], function() { // the options that show up in the select2
            return Session::all()->pluck('name', 'id')->toArray();
        }, function($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('sessions', function ($query) use ($value) {
                    $query->where('session_id', $value);
                });
            }
        });

    }

    protected function setupShowOperation()
    {
        //CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */

        CRUD::addColumn([
            'label' => 'Session',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'name',
        ]);

        CRUD::addColumn([
            'label' => 'Category',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'categories',
            'attribute' => 'name',
        ]);

        CRUD::addColumn([
            'label' => 'Date',
            'type' => 'date',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'start_date',
        ]);


        CRUD::addColumn([ // Repeater field
            'name' => 'options',
            'label' => 'Formations',
            'type' => 'table',
            'entity_singular' => 'option',
            'columns' => [
                'title' => 'Titre',
                'nb_hours' => 'Nb. heures',
                'description' => 'Description',
            ],
            'max' => 10,
        'min' => 1
        ]);


    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(FormationV2Request::class);

        //CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */

        CRUD::addField([    // Relationship
            'label'     => 'Session <span class="badge badge-warning"></span>',
            'type'      => 'relationship',
            'name'      => 'sessions',
            'entity'    => 'sessions',
            'attribute' => 'name',
            //'model'             => "App\Models\Tag",
            // 'tab'       => 'Relationships',
            'ajax'      => true,
            // 'inline_create' => true, // TODO: make it work like this too
            'inline_create'     => [
                'entity'      => 'session',
                'modal_class' => 'modal-dialog modal-xl',
            ],
            'data_source'       => backpack_url('formation/fetch/session'),
            // 'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);

        CRUD::addField([    // Relationship
            'label'     => 'Categorie',
            'type'      => 'relationship',
            'name'      => 'categories',
            'entity'    => 'categories',
            'attribute' => 'name',
            //'model'             => "App\Models\Tag",
            // 'tab'       => 'Relationships',
            'ajax'      => true,
            // 'inline_create' => true, // TODO: make it work like this too
            'inline_create'     => [
                'entity'      => 'category',
                'modal_class' => 'modal-dialog modal-xl',
            ],
            'data_source'       => backpack_url('formation/fetch/category'),
            // 'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);


        CRUD::addField([   // repeatable
            'name'  => 'options',
            'label' => 'Formations',
            'type'  => 'repeatable',
            'store_in' => 'options',
            'fields' => [
                [
                    'name'    => 'title',
                    'type'    => 'text',
                    'label'   => 'Titre',
                    'wrapper' => ['class' => 'form-group col-md-8'],
                ],
                [
                    'name'    => 'nb_hours',
                    'type'    => 'number',
                    'label'   => 'Nb. heure',
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
                [
                    'name'    => 'description',
                    'type'    => 'ckeditor',
                    'label'   => 'Description',
                    'wrapper' => ['class' => 'form-group col-md-12'],
                ]

            ],

            // optional
            'new_item_label'  => 'Autre Formation', // customize the text of the button
        ]);



//        $this->crud->addField([
//            'label' => "Profile Image",
//            'name' => "image",
//            'type' => 'image',
//            'crop' => true, // set to true to allow cropping, false to disable
//            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
//            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
//            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
//        ]);


    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function fetchSession()
    {
        return $this->fetch(\App\Models\Session::class);
    }
}
