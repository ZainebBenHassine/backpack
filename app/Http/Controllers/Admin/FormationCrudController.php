<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DummyRequest;
use App\Http\Requests\FormationRequest;
use App\Models\Category;
use App\Models\FormationSession;
use App\Models\Level;
use App\Models\Session;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;



/**
 * Class DummyCrudController.
 *
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class FormationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\FetchOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;


    public function setup()
    {


        $this->crud->setModel('App\Models\Formation');
        $this->crud->setRoute(config('backpack.base.route_prefix').'/formation');
        $this->crud->setEntityNameStrings('formation', 'formations');
    }



    /**
     * @return \Backpack\CRUD\app\Http\Controllers\Operations\Illuminate\Database\Eloquent\Collection|
     * \Backpack\CRUD\app\Http\Controllers\Operations\Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     *
     * This method is used to fetch a session when creating a new training
     */
    public function fetchSession()
    {
        return $this->fetch(\App\Models\Session::class);
    }


    /**
     * @return \Backpack\CRUD\app\Http\Controllers\Operations\Illuminate\Database\Eloquent\Collection|
     * \Backpack\CRUD\app\Http\Controllers\Operations\Illuminate\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     *
     *  fetch a category when creating a new training
     */
    public function fetchCategory()
    {
        return $this->fetch(\App\Models\Category::class);
    }


    /**
     * list all the training
     */
    protected function setupListOperation()
    {
        /**
         * List columns
         */
        \Debugbar::info($this->crud);
        CRUD::addColumn([
            'label' => 'Session',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'name' ,

        ]);

        CRUD::addColumn([
            'label' => 'Date de début',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'start_date' ,
        ]);

        CRUD::addColumn([
            'label' => 'Catégorie',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'categories',
            'attribute' => 'name',
        ]);

        CRUD::addColumn([
            'label'     => 'Niveau',
            'type'      => 'relationship',
            'name'      => 'levels',
            'entity'    => 'levels',
            'attribute' => 'level',
        ]);

        CRUD::addColumn( [
            'name' => 'title',
            'label' => 'Titre',
            'type' => 'text',
        ]);


        CRUD::addColumn( [
            'name' => 'description',
            'label' => 'Description',
            'type' => 'textarea',
        ]);

        CRUD::addColumn( [
            'name' => 'nb_hours',
            'label' => 'Nb. d\'heure',
            'type' => 'number',
        ]);


        CRUD::column('status')
            ->type('boolean')
            ->label('Draft')
            ->options([0 => 'Non', 1 => 'Oui'])
            ->wrapper([
                'element' => 'span',
                'class'   => static function ($crud, $column, $entry) {
                    return 'badge badge-'.($entry->{$column['name']} ? 'success' : 'danger');
                },
            ]);

        CRUD::addColumn( [
            'name' => 'agree',
            'label' => 'Acceptation',
            'type' => 'check',
        ]);

        $this->crud->addColumn([   // Address
            'name'  => 'address',
            'label' => 'Adresse',
            'type'  => 'address_algolia',
        ]);


        $this->crud->addColumn([
            'name' => 'image' ,
            'label'=>'Image',
            'type' => 'image',
        ]);


        $this->crud->addColumn([
            'name'  => 'video', // The db column name
            'label' => 'Video', // Table column heading
            'type'  => 'video',
        ]);


        $this->crud->addColumn([   // Upload
            'name'      => 'files',
            'label'     => 'Fichiers téléchargés',
            'type'      => 'upload_multiple',
        ]);

        $this->crud->addColumn([
            'name'     => 'created_at',
            'label'    => 'Created At',
            'type'     => 'closure',
            'function' => function ($entry) {
                return 'Ajoutée le '.$entry->created_at;
            },
        ]);

        /**
         *
         *  Create filters
         * By created_at
         */

        $this->crud->addFilter([
            'label' => 'Date de création',
            'type' => 'date_range',
            'name' => 'created_at',
            'date_range_options' => [
                'timePicker' => true,
                'locale' => ['format' => 'DD/MM/YYYY HH:mm']
            ],
        ],
            false,
            function ($value) {
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from . " 00:00:00");
                $this->crud->addClause('where', 'created_at', '<=', $dates->to . " 23:59:59");
                //Or
                //$this->crud->addClause('where', 'created_at', '<=',date("Y-m-d", strtotime($dates->to) + (3600*24) ));
            });



        /**
         *
         *  Create a dropdown filter by categories
         */
        $this->crud->addFilter([
            'name'  => 'categories',
            'type'  => 'dropdown',
            'label' => 'Catégories_V1'
        ],
            function() { // the options that show up in the select2
                return Category::all()->pluck('name', 'id')->toArray();
            },
            function($value) { // if the filter is active
                $this->crud->query = $this->crud->query->whereHas('categories', function ($query) use ($value) {
                    $query->where('category_id', $value);
                });
            });


        /**
         *
         *  Create filter by sessions
         */
        $this->crud->addFilter([
            'name'  => 'sessions',
            'type'  => 'select2_multiple',
            'label' => 'Sessions'
        ], function() { // the options that show up in the select2
            return Session::all()->pluck('name', 'id')->toArray();
        }, function($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('sessions', function ($query) use ($value) {
                    $query->where('session_id', $value);
                });
            }
        });


        /**
         *
         *  Create a select2 filter by levels
         */
        $this->crud->addFilter([
            'name'  => 'levels',
            'type'  => 'select2_multiple',
            'label' => 'Niveau'
        ],
            function()
            { // the options that show up in the select2
                           return Level::all()->pluck('level', 'id')->toArray();
        }, function($values) { // if the filter is active
            foreach (json_decode($values) as $key => $value) {
                $this->crud->query = $this->crud->query->whereHas('levels', function ($query) use ($value) {
                    $query->where('level_id', $value);
                });
            }
        });

        /**
         * Details Row
         */

        $this->crud->enableExportButtons();
        $this->crud->enableBulkActions();
        $this->crud->orderBy('created_at');
        $this->crud->setListContentClass('col-md-12');

    }


    /**
     * Show a single training object
     */
    protected function setupShowOperation()
    {
        CRUD::addColumn([
            'label' => 'Session',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'name' ,

        ]);

        CRUD::addColumn([
            'label' => 'Date de début',
            'type' => 'relationship',
            'name' => 'sessions',
            'entity' => 'sessions',
            'attribute' => 'start_date' ,
        ]);

        CRUD::addColumn([
            'label' => 'Catégorie',
            'type' => 'relationship',
            'name' => 'categories',
            'entity' => 'categories',
            'attribute' => 'name',
        ]);

        CRUD::addColumn([
            'label'     => 'Niveau',
            'type'      => 'relationship',
            'name'      => 'levels',
            'entity'    => 'levels',
            'attribute' => 'level',
        ]);

        CRUD::addColumn( [
            'name' => 'title',
            'label' => 'Titre',
            'type' => 'text',
        ]);

        CRUD::addColumn( [
            'name' => 'description',
            'label' => 'Description',
            'type' => 'textarea',
        ]);

        CRUD::addColumn([
            'name'        => 'status',
            'label'       => 'Status',
            'type'        => 'radio',
            'options'     => [
                0 => 'Draft',
                1 => 'Published'
            ]
        ]);

        CRUD::addColumn( [
            'name' => 'agree',
            'label' => 'Acceptation',
            'type' => 'check',
        ]);

        CRUD::addColumn( [
            'name' => 'nb_hours',
            'label' => 'Nb. d\'heure',
            'type' => 'number',
        ]);

        $this->crud->addColumn([
            'name' => 'image' ,
            'label'=>'Image',
            'type' => 'image',
        ]);
        $this->crud->addColumn([
            'name'  => 'video', // The db column name
            'label' => 'Video', // Table column heading
            'type'  => 'video',
        ]);


        $this->crud->addColumn([   // Upload
            'name'      => 'files',
            'label'     => 'Fichiers téléchargés',
            'type'      => 'upload_multiple',
        ]);

        $this->crud->addColumn([
            'name'     => 'created_at',
            'label'    => 'Ajoutée le',
            'type'     => 'closure',
            'function' => function ($entry) {
                return 'Ajoutée le '.$entry->created_at;
            },
        ]);

        $this->crud->addColumn([   // Address
            'name'  => 'address',
            'label' => 'Address_algolia (saved in db as string)',
            'type'  => 'address_algolia',
        ]);

    }


    /**
     * Create a new Training object
     */
    protected function setupCreateOperation()
    {
        $this->crud->setValidation(FormationRequest::class);
        $this->crud->setOperationSetting('contentClass', 'col-md-12');


        CRUD::addField([    // Relationship
            'label'     => 'Session <span class="badge badge-warning"></span>',
            'type'      => 'relationship',
            'name'      => 'sessions',
            'entity'    => 'sessions',
            'attribute' => 'name',
            //'model'             => "App\Models\Tag",
            // 'tab'       => 'Relationships',
            'ajax'      => true,
            // 'inline_create' => true, // TODO: make it work like this too
            'inline_create'     => [
                'entity'      => 'session',
                'modal_class' => 'modal-dialog modal-xl',
            ],
            'data_source'       => backpack_url('formation/fetch/session'),
            'tab'   => 'infos',
            // 'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);

        CRUD::addField([    // Relationship
            'label'     => 'Catégorie',
            'type'      => 'relationship',
            'name'      => 'categories',
            'entity'    => 'categories',
            'attribute' => 'name',
            //'model'             => "App\Models\Tag",
            // 'tab'       => 'Relationships',
            'ajax'      => true,
            // 'inline_create' => true, // TODO: make it work like this too
            'inline_create'     => [
                'entity'      => 'category',
                'modal_class' => 'modal-dialog modal-xl',
            ],
            'data_source'       => backpack_url('formation/fetch/category'),
            'tab'   => 'infos',
            // 'wrapperAttributes' => ['class' => 'form-group col-md-12'],
        ]);

        CRUD::addField([
            'label'     => 'Niveau',
            'type'      => 'checklist',
            'name'      => 'levels',
            'entity'    => 'levels',
            'attribute' => 'level',
            'model'     => "App\Models\Level",
            'pivot'     => true,
            'tab'   => 'infos',
        ]);

        CRUD::addField([   // radio
            'name'        => 'status', // the name of the db column
            'label'       => 'Draft', // the input label
            'type'        => 'radio',
            'tab'   => 'infos',
            'options'     => [
                // the key will be stored in the db, the value will be shown as label;
                1 => "Oui",
               0 => "Non",
            ],
            // optional
            'inline'      => true, // show the radios all on the same line?
        ]);

        CRUD::addField([
            'label'     => 'J\'accepte les conditions générales',
            'type'      => 'checkbox',
            'name' => 'agree',
            'tab'   => 'infos',
        ]);



//        CRUD::addField([
//            'name'      => 'upload_multiple',
//            'label'     => 'Upload multiple',
//            'type'      => 'upload_multiple',
//            'upload'    => true,
//            'disk'      => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
//            // optional:
//            'temporary' => 10, // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
//            'tab' => 'infos'
//        ]);


        CRUD::addField( [
            'name' => 'title',
            'label' => 'Titre',
            'type' => 'text',
            'tab'   => 'Formation',
        ]);

        CRUD::addField( [
            'name' => 'description',
            'label' => 'Description',
            'type' => 'ckeditor',
            'tab'   => 'Formation',
        ]);

        CRUD::addField( [
            'name' => 'nb_hours',
            'label' => 'Nb. d\'heure',
            'type' => 'number',
            'tab'   => 'Formation',
            //'prefix' => ('heures'),
            'suffix' => ("<i class='la la-clock'></i>")
        ]);

        $this->crud->addField([
            'label' => "Téléchargez une image",
            'name' => "image",
            'type' => 'image',
            'tab'   => 'Fichiers',
            'crop' => true, // set to true to allow cropping, false to disable
            'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);

        $this->crud->addField(   [   // URL
            'name'              => 'video',
            'label'             => 'Téléchargez un Vidéo - URL de video sur Youtube ou Vimeo',
            'type'              => 'video',
            'tab'               => 'Fichiers',
            'wrapperAttributes' => ['class' => 'form-group col-md-5'],
        ]);

        $this->crud->addField( [   // Browse multiple
            'name'          => 'files',
            'label'         => 'Téléchargez des fichiers',
            'type'          => 'browse_multiple',
            'tab'               => 'Fichiers',
             'multiple'   => true, // enable/disable the multiple selection functionality
            'sortable'   => false, // enable/disable the reordering with drag&drop
             'mime_types' => null, // visible mime prefixes; ex. ['image'] or ['application/pdf']
        ]);


        $this->crud->addField([   // Address
            'name'  => 'address',
            'label' => 'Address',
            'type'  => 'address_algolia',
            'fake'  => true,
            // optional
            // 'store_as_json' => true,
            'tab'           => 'Formation',
        ]);

//        $this->crud->addField([   // CustomHTML
//            'name' => 'metas_separator',
//            'type' =>  backpack_url('elfinder'),
//            'value' => '<br><h2>'.trans('backpack::crud.file_manager').'</h2><hr>',
//        ]);




//        CRUD::addFields([
//            [ // Text
//                'label'     => 'Session <span class="badge badge-warning"></span>',
//                'type'      => 'relationship',
//                'name'      => 'sessions',
//                'entity'    => 'sessions',
//                'attribute' => 'name',
//                'fake'  => true,
//                'tab'   => 'Informations Générales',
//                'ajax'      => true,
//                // 'inline_create' => true, // TODO: make it work like this too
//                'inline_create'     => [
//                    'entity'      => 'session',
//                    'modal_class' => 'modal-dialog modal-xl',
//                ],
//                'data_source'       => backpack_url('formation/fetch/session'),
//
//            ],
//            [ // Text
//                'label'     => 'Catégorie',
//                'type'      => 'relationship',
//                'name'      => 'categories',
//                'entity'    => 'categories',
//                'attribute' => 'name',
//                //'model'             => "App\Models\Tag",
//                // 'tab'       => 'Relationships',
//                'ajax'      => true,
//                // 'inline_create' => true, // TODO: make it work like this too
//                'inline_create'     => [
//                    'entity'      => 'category',
//                    'modal_class' => 'modal-dialog modal-xl',
//                ],
//                'data_source'       => backpack_url('formation/fetch/category'),
//                'fake'  => true,
//                'tab'   => 'Informations Générales',
//            ],
//            [ // Text
//                'label'     => 'Niveau',
//                'type'      => 'checklist',
//                'name'      => 'levels',
//                'entity'    => 'levels',
//                'attribute' => 'level',
//                'model'     => "App\Models\Level",
//                'pivot'     => true,
//                'fake'  => true,
//                'tab'   => 'Informations Générales',
//            ]
//        ]);
//
//
//        CRUD::addFields([
//            [ // Text
//                'name'  => 'title',
//                'label' => 'Titre',
//                'type'  => 'text',
//                'fake'  => true,
//                'tab'   => 'Formation',
//            ],
//            [ // Text
//                'name'  => 'description',
//                'label' => 'Description',
//                'type'  => 'ckeditor',
//                'fake'  => true,
//                'tab'   => 'Formation',
//            ],
//            [ // Text
//                'name'  => 'nb_hours',
//                'label' => 'Nombre d\'heures',
//                'type'  => 'number',
//                'fake'  => true,
//                'tab'   => 'Formation',
//            ],
//            [ // Text
//                'name'  => 'image',
//                'label' => 'Image',
//                'type'  => 'image',
//                'crop' => true, // set to true to allow cropping, false to disable
//                'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
//                'fake'  => true,
//                'tab'   => 'Formation',
//            ],
//            [   // URL
//                'name'              => 'video',
//                'label'             => 'Video - link to video file on Youtube or Vimeo',
//                'type'              => 'video',
//                'tab'               => 'Formation',
//                'wrapperAttributes' => ['class' => 'form-group col-md-5'],
//            ],
//
//        ]);

    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->crud->hasAccessOrFail('create');

        // execute the FormRequest authorization and validation, if one is required
        $request = $this->crud->validateRequest();

        // insert item in the db
        $item = $this->crud->create($this->crud->getStrippedSaveRequest());
        $this->data['entry'] = $this->crud->entry = $item;

        // show a success message
        \Alert::success('Formation ajoutée avec succès')->flash();

        // save the redirect choice for next time
        $this->crud->setSaveAction();

        return $this->crud->performSaveAction($item->getKey());
    }

    /**
     * @return \Illuminate\Http\Response
     */


    /**
     * Update an existing Training object
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

    }

    public function updateCrud(UpdateRequest $request = null)
    {
        $this->crud->hasAccessOrFail('update');

        // fallback to global request instance
        if (is_null($request)) {
            $request = \Request::instance();
        }

        // replace empty values with NULL, so that it will work with MySQL strict mode on
        foreach ($request->input() as $key => $value) {
            if (empty($value) && $value !== '0') {
                $request->request->set($key, null);
            }
        }

        // update the row in the db

                  $this->crud->entry = $this->crud->update($request->get($this->crud->model->getKeyName()),
                $request->except('redirect_after_save', '_token'));

        // show a success message
        \Alert::success('ffgff')->flash();

        return \Redirect::to($this->crud->route);
    }

}
