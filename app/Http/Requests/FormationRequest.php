<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class FormationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'description' => 'required',
              'nb_hours' => 'required | numeric',
              'image' => 'required',
              'image.*' => 'image|mimes:jpeg,png,jpg',
              'sessions' => 'required',
              'categories' => 'required',
            'levels' => 'required'

           ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return
            [
                'title.required' => 'Merci de saisir le titre de la formation !',
                'nb_hours.required' => 'Merci de saisir le nombre d\'heure de la formation !',
                'description.required' => 'Merci de saisir une description !',
                'sessions.required' => 'Merci de sélectionner  une session !',
                'levels.required' => 'Merci de sélectionner  un ou plusieurs niveaux pour votre frmation !',
                'categories.required' => 'Merci de sélectionner  une catégorie !',
        ];
    }
}
