<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Foundation\Http\FormRequest;

class FormationV2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sessions' => 'required',
            'categories' => 'required',
//            'options' => ['array', 'min:1'],
//            'options.*.title' => ['required'],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return
            [
                'sessions.required' => 'Merci de sélectionner  une session !',
                'categories.required' => 'Merci de sélectionner  une catégorie !',

//                'options.*.title' => 'Merci de saisir le titre de la formation !',
//                'nb_hours.required' => 'Merci de saisir le nombre d\'heure de la formation !',
//                'description.required' => 'Merci de saisir une description !',
            ];
    }
}
