<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Spatie\Permission\Traits\HasRoles;

class FormationV2 extends Model
{
    use CrudTrait;
    use HasRoles;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'formation_v2s';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

//    public function setImageAttribute($value)
//    {
//        if (app('env') == 'production') {
//            \Alert::warning('In the online demo the images don\'t get uploaded.');
//
//            return true;
//        }
//
//        $attribute_name = 'image';
//        $disk = 'uploads'; // use Backpack's root disk; or your own
//        $destination_path = 'Images';
//
//        // if the image was erased
//        if ($value == null) {
//            // delete the image from disk
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//
//            // set null in the database column
//            $this->attributes[$attribute_name] = null;
//        }
//
//        // if a base64 was sent, store it in the db
//        if (Str::startsWith($value, 'data:image')) {
//            // 0. Make the image
//            $image = \Image::make($value)->encode('jpg', 90);
//
//            // 1. Generate a filename.
//            $filename = md5($value.time()).'.jpg';
//
//            // 2. Store the image on disk.
//            \Storage::disk($disk)->put($destination_path.'/'.$filename, $image->stream());
//
//            // 3. Delete the previous image, if there was one.
//            \Storage::disk($disk)->delete($this->{$attribute_name});
//
//            // 4. Save the public path to the database
//            // but first, remove "public/" from the path, since we're pointing to it from the root folder
//            // that way, what gets saved in the database is the user-accesible URL
//            $public_destination_path = Str::replaceFirst('public/', '', $destination_path);
//            $this->attributes[$attribute_name] = '/uploads/'.$public_destination_path.'/'.$filename;
//        }
//    }


    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "uploads";
        $destination_path = "Images";

        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }


    public function uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path)
    {
        $request = \Request::instance();
        $attribute_value = (array) $this->{$attribute_name};
        $files_to_clear = $request->get('clear_'.$attribute_name);
        // if a file has been marked for removal,
        // delete it from the disk and from the db
        if ($files_to_clear) {
            $attribute_value = (array) $this->{$attribute_name};
            foreach ($files_to_clear as $key => $filename) {
                \Storage::disk($disk)->delete($filename);
                $attribute_value = array_where($attribute_value, function ($value, $key) use ($filename) {
                    return $value != $filename;
                });
            }
        }
        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name)) {
            foreach ($request->file($attribute_name) as $file) {
                if ($file->isValid()) {
                    // 1. Generate a new file name
                    $new_file_name =md5($file.time()).'.jpg';
                    // 2. Move the new file to the correct path
                    $file_path = $file->storeAs($destination_path, $new_file_name, $disk);
                    // 3. Add the public path to the database
                    $attribute_value[] = 'uploads/'.$file_path;
                }
            }
        }
        $this->attributes[$attribute_name] = json_encode($attribute_value);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function sessions()
    {
        return $this->belongsToMany(\App\Models\Session::class, 'formationv2_session');
    }

    public function categories()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'category_formationv2');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'monster_tag');
    }

    public function articles()
    {
        return $this->belongsToMany(\App\Models\Article::class, 'monster_article');
    }

    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'monster_product');
    }

    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'select');
    }

    public function article()
    {
        return $this->belongsTo(\App\Models\Article::class, 'select2_from_ajax');
    }

    public function icon()
    {
        return $this->belongsTo(\App\Models\Icon::class, 'icon_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
