<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class FormationSession extends Model
{
    protected $table = 'formation_session';

    public function formations()
    {
        return $this->hasMany('App\Models\Formation', 'formation_id', 'id');
    }

    public function sessions(){
        return $this->hasMany('App\Models\Session', 'session_id', 'id');
    }
}
