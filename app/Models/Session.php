<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class Session extends Model
{
    use CrudTrait;
    use HasRoles;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'sessions';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */


    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'monster_tag');
    }

    public function formations()
    {
        return $this->belongsToMany(\App\Models\Formation::class, 'formation_session')
            ->using('App\Models\FormationSession')
            ->withPivot([
                'start_date',
                'end_date',
            ]);
    }




    public function tag()
    {
        return $this->belongsTo('App\Models\Tag', 'tag_id');
    }

    public function products()
    {
        return $this->belongsToMany(\App\Models\Product::class, 'monster_product');
    }

    public function articles()
    {
        return $this->belongsToMany(\App\Models\Article::class, 'monster_article');
    }

    public function article()
    {
        return $this->belongsTo(\App\Models\Article::class, 'select2_from_ajax');
    }

    public function icon()
    {
        return $this->belongsTo(\App\Models\Icon::class, 'icon_id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopePublished($query)
    {
        return $query->where('status', 'PUBLISHED')
            ->where('date', '<=', date('Y-m-d'))
            ->orderBy('date', 'DESC');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
